using NAudio.Wave;
using NAudio.Wave.Asio;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace audiorectest2
{

    public class Item
    {
        int id;
        string name;
        public Item() {

        }

    }
    static class Program
    {
        [STAThread]
        static void Main() {
            int channel = 0;

            var drivers = AsioOut.GetDriverNames();
            var asioOut = new AsioOut(drivers[0]);
            var channels = new List<string>();

            for (int i = 0; i < asioOut.DriverInputChannelCount; i++) {
                channels.Add(asioOut.AsioInputChannelName(i));
            }

            bool closing = false;
            WaveFileWriter writer = null;

            var f = new Form() { Font = new Font("Fira Code", 12) };
            f.Width = 800;

            var record_button = new Button() { Text = "Record", Enabled = false };
            var stop_button = new Button() { Text = "Stop", Left = record_button.Right, Enabled = false };
            var driver_label = new Label() { Text = "Select Driver...", Top = record_button.Bottom, Width = f.Right };
            var label = new Label() { Text = "Select Input...", Top = driver_label.Bottom, Width = f.Right };
            var meta_label = new Label() { Text = "Meta...", Top = label.Bottom, Width = f.Right };
            var driver_combo = new ListBox() { Top = meta_label.Bottom, Width = 300, DataSource = drivers };
            var channel_combo = new ListBox() { Left = driver_combo.Right, Top = meta_label.Bottom, Width = 250, DataSource = channels, Enabled = false };

            f.Controls.AddRange(new Control[] { record_button, stop_button });
            f.Controls.Add(driver_label);
            f.Controls.Add(label);
            f.Controls.Add(meta_label);
            f.Controls.Add(driver_combo);
            f.Controls.Add(channel_combo);

            f.Height = channel_combo.Top + channel_combo.Bottom;

            void DisableControls() {
                record_button.Enabled = false;
                stop_button.Enabled = false;
            }

            void ResetButtons() {
                record_button.Enabled = true;
                stop_button.Enabled = false;
            }

            driver_combo.SelectedIndexChanged += (s, a) => {
                f.Invoke(new Action(() => {

                    driver_label.Text = "Loading Driver...";
                    DisableControls();
                    asioOut.Stop();
                    asioOut.Dispose();

                    Task.Run(() => {
                        f.Invoke(new Action(() => {
                            asioOut = new AsioOut(drivers[driver_combo.SelectedIndex]);
                            channel = 0;
                            asioOut.InitRecordAndPlayback(null, 1, 44100);

                            ResetButtons();
                            channel_combo.Enabled = true;
                            driver_label.Text = "Driver > " + asioOut.DriverName;
                        }));
                    });
                }));
            };

            channel_combo.SelectedIndexChanged += (s, a) => {
                f.Invoke(new Action(() => {
                    asioOut.Stop();
                    channel = channel_combo.SelectedIndex;
                    asioOut.InputChannelOffset = channel;
                    // 
                    label.Text = "Channel " + channel + " - " + asioOut.AsioInputChannelName(channel);
                    asioOut.Play();

                }));
            };

            record_button.Click += (s, a) => {
                asioOut.Play();
                writer = new WaveFileWriter("dank1234.wav", new WaveFormat(44100, 1));
                record_button.Enabled = false;
                stop_button.Enabled = true;
            };

            stop_button.Click += (s, a) => {
                asioOut.Stop();
            };

            asioOut.AudioAvailable += (s, a) => {
                label.Text = "Max value => " + a.InputBuffers.Max();
                var samples = a.GetAsInterleavedSamples();

                meta_label.Text = "Writing audio => " + samples.Length;
                writer.WriteSamples(samples, 0, samples.Length);

            };

            asioOut.PlaybackStopped += (s, a) => {
                writer?.Dispose();
                writer = null;

                record_button.Enabled = true;
                stop_button.Enabled = false;

                if (closing) {
                    asioOut.Dispose();
                }
            };

            f.FormClosing += (s, a) => { closing = true; asioOut.Stop(); };
            Application.Run(f);
        }
    }
}
